﻿using System;
using System.Diagnostics;
using System.IO;

namespace Monitor
{
    class Program
    {
        static string DocumentsPath;
        static string appPath;
        static void Main(string[] args)
        {
            DocumentsPath = Properties.Settings.Default.DocumentsPath;
            
            FileSystemWatcher monitor = new FileSystemWatcher(DocumentsPath);
            monitor.NotifyFilter = (
                NotifyFilters.LastAccess    |
                NotifyFilters.LastWrite     |
                NotifyFilters.FileName      |
                NotifyFilters.DirectoryName                
                );

            
            monitor.Created += OnChange;
            
            monitor.EnableRaisingEvents = true;
            PrintInformation();
            Console.ReadLine();
        }


        public static  void PrintInformation() {
            Console.WriteLine(" ------------------------------------------------------------------------------------------------------");
            Console.WriteLine(" Monitor de documentos corriendo.\n Si se cierra esta ventanta proceda a ejecutar el programa {0}", Properties.Settings.Default.NetMonitor);
            Console.WriteLine(" ------------------------------------------------------------------------------------------------------");
        }

        private static void OnChange(object source, FileSystemEventArgs e)
        {
            WatcherChangeTypes tipoDeCambio = e.ChangeType;
            Console.WriteLine("El archivo {0} ha sido agredado", e.Name);
            appPath = Properties.Settings.Default.appPhp;
            
         
            Process process = new Process();
            process.StartInfo.FileName = "cmd.exe";
            Console.Clear();
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.RedirectStandardInput = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.UseShellExecute = false;
            process.Start();
            process.StandardInput.WriteLine(appPath);
            process.StandardInput.Flush();
            process.StandardInput.Close();
            process.WaitForExit();
            Console.WriteLine(process.StandardOutput.ReadToEnd());
            process.Start();

            PrintInformation();
        }
    }
}
